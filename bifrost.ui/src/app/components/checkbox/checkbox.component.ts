import { Component, Input, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
    selector: '[checkbox-component]',
    templateUrl: './checkbox.component.html'
})
export class CheckboxComponent implements OnInit {

    @Input() checkboxModel: any;

    ngOnInit() {
        console.log(this.checkboxModel);
    }
}