import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { CheckboxComponent,
         ElementComponent,
         FormComponent,
         HyperlinkItemComponent,
         LabelItemComponent,
         TableComponent,
         TextboxComponent,
         MenuComponent }   from './';

import { ViewDataService } from '../view-data/view-data.service';
        
const DynamicComponents = [
    CheckboxComponent,
    ElementComponent,
    FormComponent,
    HyperlinkItemComponent,
    LabelItemComponent,
    TableComponent,
    TextboxComponent,
    MenuComponent
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule
    ],
    exports: [ DynamicComponents ],
    declarations: [ DynamicComponents ],
    entryComponents: [ DynamicComponents ],
    providers: [ViewDataService]
})
export class DynamicComponentsModule {
    public components: Map<any, any>;

    constructor() {

        this.components = new Map<any, any>();

        this.components.set('textbox-component', TextboxComponent);
        this.components.set('table-component', TableComponent);
        this.components.set('label-item', LabelItemComponent);
        this.components.set('hyperlink-item', HyperlinkItemComponent);
        this.components.set('element', ElementComponent);
        this.components.set('checkbox-component', CheckboxComponent);
        this.components.set('form-component', FormComponent);
        this.components.set('menu-component', MenuComponent);
    }
}
