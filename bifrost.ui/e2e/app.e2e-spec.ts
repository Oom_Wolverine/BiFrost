import { DigitalcertificationPage } from './app.po';

describe('digitalcertification App', function() {
  let page: DigitalcertificationPage;

  beforeEach(() => {
    page = new DigitalcertificationPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
