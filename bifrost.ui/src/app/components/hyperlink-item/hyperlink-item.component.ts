import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: '[hyperlink-item]',
    templateUrl: './hyperlink-item.component.html'
})
export class HyperlinkItemComponent{

    @Input() hyperlinkModel : any;

}