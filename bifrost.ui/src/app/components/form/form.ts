export class Form {
    public component: string;
    public inputs: any[];

    constructor(formData: any) {   
        this.component = 'form-component';     
        this.inputs = formData.content;
    }
}