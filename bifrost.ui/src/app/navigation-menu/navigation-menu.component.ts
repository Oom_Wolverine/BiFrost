import { Component } from '@angular/core';
import { DigitalcertificationRoutingModule } from '../app-routing.module';

@Component({
    selector: '[navigation-menu-component]',
    templateUrl: './navigation-menu.component.html'
})
export class NavigationMenuComponent{

    appMenus = [
        { name: 'News' },
        { name: 'My Bank Accounts',
                "submenu":
                [
                   { name: 'My Accounts',
                     link : 'accounts' },
                   { name: 'My Cards',
                    link : 'cards' }
                ] },
        { name: 'Transfer' },
        { name: 'Pay' },
        { name: 'Shares' }
    ];

    menu = [
        { name: 'News' },
        { name: 'My Bank Accounts' },
        { name: 'Transfer' },
        { name: 'Pay' }
    ];

    mybankaccountsmenu = [
        { name: 'My Accounts' },
        { name: 'My Cards' }
    ];

    mycardsaccountsmenu = [
        { name: 'My Cards' },
        { name: 'My Details' }
    ];
}