import { Component, Input } from '@angular/core';


@Component({
    selector: '[label-item]',
    templateUrl: './label-item.component.html'
})

export class LabelItemComponent{

    @Input() labelModel : any;
    
}