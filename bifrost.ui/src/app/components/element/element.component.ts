import { Component, Input } from '@angular/core';

@Component({
    selector: '[element]',
    templateUrl: 'element.component.html'
})
export class ElementComponent {
    constructor() { }

    @Input() model: any;
}