import { Component, OnInit, Input }                   from '@angular/core';
import { trigger, transition, animate, style, state } from '@angular/core';
import { ViewDataService }                            from '../../view-data/view-data.service';
import { Table }                                      from './table';

@Component({
    selector: '[table-component]',
    templateUrl: './table.component.html',
    host: {
        '[@routeAnimation]': 'true',
        '[style.display]': "'block'",
        '[style.position]': "'relative'"
    },
    animations: [
        trigger('routeAnimation', [
            state('*', style({ transform: 'translateX(0)', opacity: 1 })),
            transition('void => *', [
                style({ transform: 'translateX(-100%)', opacity: 0 }),
                animate(500)
            ]),
            transition('* => void', animate(500, style({ transform: 'translateX(100%)', opacity: 0 })))
        ])
    ]
})
export class TableComponent {

    public columns: any[];
    public rows: any[];

    @Input() content: string;

    constructor() { }

    public populate(table: Table): void {
        this.columns = table.columns;
        this.rows = table.rows;
    }
}