import {
    Component,
    NgModule,
    NO_ERRORS_SCHEMA,
    ViewEncapsulation,
    Input,
    Compiler,
    ViewContainerRef,
    ReflectiveInjector,
    NgModuleFactory,
    NgModuleFactoryLoader,
    SystemJsNgModuleLoader,
    ViewChild,
    Type
} from "@angular/core";

import { DynamicComponentsModule } from '../components/dynamic-components.module';

@Component({
    selector: '[dynamic-component-loader]',
    styles: [],
    template: `
        <div>
            <span #span></span>
        </div>
        `
})
export class DynamicComponentLoader {
    @ViewChild('span', { read: ViewContainerRef }) 
    private span: any;

    constructor(
        private vcRef: ViewContainerRef,
        private compiler: Compiler
    ) { }

    @Input('source')
    set tile(value: any) {
        this.createComponent(value);
    }

    createComponent(selector: any) {
        let injector = ReflectiveInjector
            .fromResolvedProviders([], this.vcRef.parentInjector);
        // Create module loader
        this.compiler.compileModuleAsync(DynamicComponentsModule)
            .then((nmf: NgModuleFactory<any>) => {
                // create NgModuleRef
                let ngmRef = nmf.create(injector);
                let component = ngmRef.instance.components.get(selector.name);
                // Create component factory
                let cmpFactory = ngmRef
                    .componentFactoryResolver
                    .resolveComponentFactory(component);
                // Create the component
                let componentRef = this.span.createComponent(cmpFactory, 0, injector, []);
                componentRef.instance.name = selector;
                componentRef.instance.populate(selector.data);
                componentRef.changeDetectorRef.detectChanges();
                componentRef.onDestroy(() => {
                    componentRef.changeDetectorRef.detach();
                });
            });
    }
}