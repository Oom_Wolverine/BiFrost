import { Component } from '@angular/core';
import { Menu } from './menu';
import { ActivatedRoute, UrlSegment } from '@angular/router';

@Component({
    selector: '[menu-component]',
    templateUrl: 'menu.component.html'
})
export class MenuComponent {
    public links: any[];
    public heading: string;

    constructor(private activatedRoute: ActivatedRoute) {
        this.links = [];
        this.heading = "";
    }

    public populate(menu: Menu): void {
        this.links = menu.links;

        this.activatedRoute.url.subscribe((urlSegments: UrlSegment[]) => {
            var url = this.parseUrl(urlSegments);
            this.links.forEach(link => {
                var src = this.stripInitialSlash(link.src);
                if (src === url) {
                    this.heading = link.name;
                    
                    link.active = true;
                }
            });
        });
    }

    public parseUrl(urlSegments: UrlSegment[]): string {
        var url = '';
        urlSegments.forEach((segment, index) => {
            url += segment.path;
            if (index !== (urlSegments.length - 1)) {
                url += "/";
            }
        });

        return url;
    }

    public stripInitialSlash(url: string) {
        return url.substring(1, url.length);
    }
}