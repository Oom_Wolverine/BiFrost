import { Component, OnInit } from '@angular/core';
import { ViewDataService } from '../view-data/view-data.service';
import { ActivatedRoute, UrlSegment } from '@angular/router';

@Component({
    selector: '[master-component]',
    templateUrl: 'master.component.html'
})
export class MasterComponent implements OnInit {
    
    constructor(private viewDataService: ViewDataService, private activateRoute: ActivatedRoute) {
        this.components = [];
    }
    
    private components: any[];

    ngOnInit() { 
        this.activateRoute.url.subscribe((data) => {
            var contentUrl = this.parseUrl(data);
            if (!contentUrl.length) {
                contentUrl = "accounts";
            }

            this.viewDataService.getViewComponentData(contentUrl).subscribe((r) => {
                this.components = [];
                
                r.forEach(element => {
                    this.components.push({ name: element.component, data: element, priority: element.priority || -99 });
                });
                
                this.components.sort(function (a, b) {
                    return b.priority - a.priority;
                })
            });
        });
    }

    parseUrl(urlSegments: UrlSegment[]) {
        var url = '';
        urlSegments.forEach((segment, index) => {
            url += segment.path;
            if (index !== (urlSegments.length - 1)) {
                url += "/";
            }
        });

        return url;
    }
}