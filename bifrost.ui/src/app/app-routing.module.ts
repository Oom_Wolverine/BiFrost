import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterComponent } from './master/master.component';


const routes: Routes = [
  { path: '', redirectTo: '/accounts', pathMatch: 'full' },
  { path: '**', component: MasterComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class DigitalcertificationRoutingModule {

}
