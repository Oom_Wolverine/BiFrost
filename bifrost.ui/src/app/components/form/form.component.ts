import { Component, Input } from '@angular/core';
import { trigger, transition, animate, style, state } from '@angular/core';
import { ViewDataService }   from '../../view-data/view-data.service';
import { Form }              from './form';
import { Table }             from '../table/table';
import { ActivatedRoute }      from '@angular/router';
import { Location }            from '@angular/common';

@Component({
    selector: '[form-component]',
    templateUrl: './form.component.html',
    host: {
        '[@routeAnimation]': 'true',
        '[style.display]': "'block'",
        '[style.position]': "'relative'"
    },
    animations: [
        trigger('routeAnimation', [
            state('*', style({ transform: 'translateX(0)', opacity: 1 })),
            transition('void => *', [
                style({ transform: 'translateX(-100%)', opacity: 0 }),
                animate(500)
            ]),
            transition('* => void', animate(500, style({ transform: 'translateX(100%)', opacity: 0 })))
        ])
    ]
})
export class FormComponent {

    public inputs: any[];
    public itemId: any;

    constructor(private viewDataService: ViewDataService, route: ActivatedRoute, private location: Location) {
        this.inputs = [];
        this.itemId = route.snapshot.params['id'];
    }

    back(){
        this.location.back();
    }

    private populate(form: Form): void {
        this.inputs = form.inputs;
        console.log(this.inputs);
    }
}