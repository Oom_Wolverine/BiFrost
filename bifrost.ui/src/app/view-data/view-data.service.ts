import { Injectable }       from '@angular/core';
import { Response, Http }   from '@angular/http';
import { Observable }       from 'rxjs/Observable';
import { Table }            from '../components/table/table';
import { Form }             from '../components/form/form';
import { Menu }             from '../components/menu/menu';
import { Router }         from '@angular/router'

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class ViewDataService {

    constructor(private http: Http, private router: Router) { }

    public getViewComponentData(url: string): Observable<any[]> {
        switch (url) {
            case 'accounts':
                return this.http.get('app/mocks/table.data.mock.accounts.json')
                    .map(res => this.ParseComponents(res));
            case 'cards':
                return this.http.get('app/mocks/table.data.mock.cards.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/1':
                return this.http.get('app/mocks/form.data.mock.card.1.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/2':
                return this.http.get('app/mocks/form.data.mock.card.2.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/3':
                return this.http.get('app/mocks/form.data.mock.card.3.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/4':
                return this.http.get('app/mocks/form.data.mock.card.4.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/5':
                return this.http.get('app/mocks/form.data.mock.card.5.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/1':
                return this.http.get('app/mocks/form.data.mock.account.1.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/2':
                return this.http.get('app/mocks/form.data.mock.account.2.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/3':
                return this.http.get('app/mocks/form.data.mock.account.3.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/4':
                return this.http.get('app/mocks/form.data.mock.account.4.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/1/history':
                return this.http.get('app/mocks/table.data.mock.account.history.1.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/2/history':
                return this.http.get('app/mocks/table.data.mock.account.history.2.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/3/history':
                return this.http.get('app/mocks/table.data.mock.account.history.3.json')
                    .map(res => this.ParseComponents(res));
            case 'accounts/4/history':
                return this.http.get('app/mocks/table.data.mock.account.history.4.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/1/history':
                return this.http.get('app/mocks/table.data.mock.card.history.1.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/2/history':
                return this.http.get('app/mocks/table.data.mock.card.history.2.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/3/history':
                return this.http.get('app/mocks/table.data.mock.card.history.3.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/4/history':
                return this.http.get('app/mocks/table.data.mock.card.history.4.json')
                    .map(res => this.ParseComponents(res));
            case 'cards/5/history':
                return this.http.get('app/mocks/table.data.mock.card.history.5.json')
                    .map(res => this.ParseComponents(res));
            default:
                return Observable.of([]);


        }

        // if (url.indexOf('cards/1') > -1) {
        //     return this.http.get('app/mocks/form.data.mock.account.1.json')
        //         .map(res => this.ParseComponents(res));
        // }


    }

    private ParseComponents(res: Response): any[] {
        let data = res.json();
        let components = [];

        data.forEach(componentJson => {
            switch (componentJson.name) {
                case 'Table':
                    components.push(new Table(componentJson));
                    break;
                case 'Form':
                    components.push(new Form(componentJson));
                    break;
                case 'Menu':
                    components.push(new Menu(componentJson));
                    break;
                default:
                    break;
            }
        });

        return components;
    }
}