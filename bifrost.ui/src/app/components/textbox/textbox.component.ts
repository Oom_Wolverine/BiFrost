import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: '[textbox-component]',
    templateUrl: './textbox.component.html'
})
export class TextboxComponent {
    @Input() value: string;
}