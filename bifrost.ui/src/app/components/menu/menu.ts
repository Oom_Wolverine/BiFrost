export class Menu {
    public links: any[];
    public component = "menu-component";
    public priority = 99;

    constructor(links: any) {
        this.links = links.links;
    }
}