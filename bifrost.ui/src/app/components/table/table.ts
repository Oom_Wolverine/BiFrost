export class Table {
    public component: string;
    public columns: any[];
    public rows: any[];

    constructor(tableData: any) {
        this.component = 'table-component';      
        this.columns = tableData.columns;
        this.rows = tableData.rows; 
    }
}