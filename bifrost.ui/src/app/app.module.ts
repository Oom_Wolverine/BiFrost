import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Input, trigger, state, style, transition, animate } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
import { ViewDataService} from './view-data/view-data.service';
import { DigitalcertificationRoutingModule} from './app-routing.module';
import { DynamicComponentLoader } from './components/dynamic-component-loader';
import { DynamicComponentsModule } from './components/dynamic-components.module';
import { MasterComponent } from './master/master.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationMenuComponent,
    DynamicComponentLoader,
    MasterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DigitalcertificationRoutingModule,
    DynamicComponentsModule   
  ],
  providers: [ViewDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
